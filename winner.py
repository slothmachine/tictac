t = []
t.append(['X', ' ', 'X'])
t.append([' ', 'O', ' '])
t.append(['X', 'O', 'O'])

def winner():
    win_combinations = [
        [t[0][0], t[1][1], t[2][2]],
        [t[0][2], t[1][1], t[2][0]],

        [t[0][0], t[0][1], t[0][2]],
        [t[1][0], t[1][1], t[1][2]],
        [t[2][0], t[2][1], t[2][2]],

        [t[0][0], t[1][0], t[2][0]],
        [t[0][1], t[1][1], t[2][1]],
        [t[0][2], t[1][2], t[2][2]]
    ]

    for i in range(len(win_combinations)):
        result = reduce(compare, win_combinations[i])
        if result != ' ':
            return result

    return None

def compare(i, j):
    if i == j:
        return i
    else:
        return ' '

print winner()
